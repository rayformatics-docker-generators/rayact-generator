# RAYACT-GENERATOR

This project is a generator of react project using docker & docker-compose.

React project use `react-ts`, `vite` & `pnpm`

Node Version: `current-slim`

## Requirements

- docker
- docker-compose

## How to install

Open `.env` file and modify variables value :

### Project configuration
`PROJECT_NAME`: Is the name of project (Update container_name, directory of project in container)

`PROJECT_PORT`: Is the service port u want to use for project
