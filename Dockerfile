FROM node:current-alpine

ARG LOCAL_USER=project
ARG PROJECT_NAME=project
ENV PROJECT_NAME=$PROJECT_NAME
ENV LOCAL_USER=$LOCAL_USER
ENV PROJECT_PATH=/opt/$PROJECT_NAME

WORKDIR $PROJECT_PATH

RUN adduser -D -h $PROJECT_PATH $LOCAL_USER && \
    mkdir -p $HOME && \
    chown -R $LOCAL_USER:$LOCAL_USER $PROJECT_PATH && \
    apk add bash


# INIT PROJECT
RUN mkdir /opt/new_app && \
cd /opt/new_app && npm -g install pnpm && \
npx create-vite . --template react-ts && \
chown -hR $LOCAL_USER:$LOCAL_USER /opt/new_app

COPY ./app/entrypoint.sh $PROJECT_PATH
RUN chown -hR $LOCAL_USER:$LOCAL_USER $PROJECT_PATH

WORKDIR $PROJECT_PATH

SHELL ["/bin/bash", "-c"]

CMD if [ $(ls /opt/$PROJECT_NAME | wc -l) -gt 2 ]; then cd /opt/$PROJECT_NAME && bash ./entrypoint.sh; else cp -r /opt/new_app/* $PROJECT_PATH && cd /opt/$PROJECT_NAME && bash ./entrypoint.sh ; fi
